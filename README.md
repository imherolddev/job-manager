## Project Definition

Job management application intended to assist in keeping track of working hours, and keeping daily logs. As a construction worker by day, I have come to appreciate the advancements and convenience that technology has brought to the workplace. I have a love for Android and the beauty of app design, as such I would like to bring my skill and vision to the users who would benefit from a project management type of application. I intend for this to be as full featured as possible, covering all bases of an Android devices capabilities. Future plans would include full web integration.

## Competitive Overview

Time Recording – DynamicG – This is the application I currently use to track my working hours. I have paid for the premium version and enjoy its features very much.

Hours Tracker – cribasoft, LLC – This is the application used by my co-workers with iPhones use to track their working hours. I was intrigued by the design of this app.

My application will stand out due to my unique, clean, and modern UI. I will also be adding extra functionality to encapsulate project management for any line of work or even personal use.

## Scope of Work

Android application intended assist in tracking hours on a project. Will also allow a user to make daily logs of work done, to document progress and any issues. These items will be able to be organized and shared.
Data will be stored in a local SQLite database for the time being. A DAO interface will be used for easy interchanging of database types. The end goal would be to host data on a MySQL cloud server.

## Milestones

	Week 1
	*Statement of Work
	UI and pseudo code, to begin formulating UX

	Week 2
	UI and pseudo code. Design some use case diagrams based on assumed UX
	MySQL database Entity Relationship Model, and subsequent Data Access Object Design
	*Design document - class diagram, sequence diagram

	Week 3
	State diagrams for UX and database access scenarios
	Begin converting UI and pseudo code work to Android layouts
	Begin writing database helper utility

	Week 4
	Write code to bind data to UI elements
	Test input and output of database

	Week 5
	Consider and write up a deployment plan
	Troubleshooting and continue testing
	Prepare final documents
	
	Week 6
	Finalize testing to prepare for deployment
	Prepare final documents and Play Store listings