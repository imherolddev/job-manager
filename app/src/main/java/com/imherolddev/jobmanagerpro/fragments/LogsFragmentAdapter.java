package com.imherolddev.jobmanagerpro.fragments;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;

import com.imherolddev.jobmanagerpro.R;
import com.imherolddev.jobmanagerpro.model.Job;
import com.imherolddev.jobmanagerpro.model.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by imherolddev on 9/22/2014.
 */
public class LogsFragmentAdapter extends BaseExpandableListAdapter {

    private Activity activity;
    private ArrayList<Job> jobArrayList;
    private ArrayList<Log> logArrayList;

    private LayoutInflater layoutInflater;

    public LogsFragmentAdapter(Activity activity, ArrayList<Job> jobArrayList, ArrayList<Log> logArrayList) {

        this.activity = activity;
        this.jobArrayList = jobArrayList;
        this.logArrayList = logArrayList;

        this.jobArrayList = getJobArrayList();

        layoutInflater = activity.getLayoutInflater();

    }

    /**
     * Gets the number of groups.
     *
     * @return the number of groups
     */
    @Override
    public int getGroupCount() {
        return jobArrayList.size();
    }

    /**
     * Gets the number of children in a specified group.
     *
     * @param groupPosition the position of the group for which the children
     *                      count should be returned
     * @return the children count in the specified group
     */
    @Override
    public int getChildrenCount(int groupPosition) {

        return jobArrayList.get(groupPosition).getLogArrayList().size();

    }

    /**
     * Gets the data associated with the given group.
     *
     * @param groupPosition the position of the group
     * @return the data child for the specified group
     */
    @Override
    public Object getGroup(int groupPosition) {
        return jobArrayList.get(groupPosition);
    }

    /**
     * Gets the data associated with the given child within the given group.
     *
     * @param groupPosition the position of the group that the child resides in
     * @param childPosition the position of the child with respect to other
     *                      children in the group
     * @return the data of the child
     */
    @Override
    public Object getChild(int groupPosition, int childPosition) {

        return jobArrayList.get(groupPosition).getLogArrayList().get(childPosition);

    }

    /**
     * Gets the ID for the group at the given position. This group ID must be
     * unique across groups. The combined ID (see
     * {@link #getCombinedGroupId(long)}) must be unique across ALL items
     * (groups and all children).
     *
     * @param groupPosition the position of the group for which the ID is wanted
     * @return the ID associated with the group
     */
    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    /**
     * Gets the ID for the given child within the given group. This ID must be
     * unique across all children within the group. The combined ID (see
     * {@link #getCombinedChildId(long, long)}) must be unique across ALL items
     * (groups and all children).
     *
     * @param groupPosition the position of the group that contains the child
     * @param childPosition the position of the child within the group for which
     *                      the ID is wanted
     * @return the ID associated with the child
     */
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    /**
     * Indicates whether the child and group IDs are stable across changes to the
     * underlying data.
     *
     * @return whether or not the same ID always refers to the same object
     */
    @Override
    public boolean hasStableIds() {
        return false;
    }

    /**
     * Gets a View that displays the given group. This View is only for the
     * group--the Views for the group's children will be fetched using
     * {@link #getChildView(int, int, boolean, android.view.View, android.view.ViewGroup)}.
     *
     * @param groupPosition the position of the group for which the View is
     *                      returned
     * @param isExpanded    whether the group is expanded or collapsed
     * @param convertView   the old view to reuse, if possible. You should check
     *                      that this view is non-null and of an appropriate type before
     *                      using. If it is not possible to convert this view to display
     *                      the correct data, this method can create a new view. It is not
     *                      guaranteed that the convertView will have been previously
     *                      created by
     *                      {@link #getGroupView(int, boolean, android.view.View, android.view.ViewGroup)}.
     * @param parent        the parent that this view will eventually be attached to
     * @return the View corresponding to the group at the specified position
     */
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.listrow_group, parent, false);
        }

        ((CheckedTextView) convertView).setText(jobArrayList.get(groupPosition).getJobName());
        ((CheckedTextView) convertView).setChecked(isExpanded);

        return convertView;

    }

    /**
     * Gets a View that displays the data for the given child within the given
     * group.
     *
     * @param groupPosition the position of the group that contains the child
     * @param childPosition the position of the child (for which the View is
     *                      returned) within the group
     * @param isLastChild   Whether the child is the last child within the group
     * @param convertView   the old view to reuse, if possible. You should check
     *                      that this view is non-null and of an appropriate type before
     *                      using. If it is not possible to convert this view to display
     *                      the correct data, this method can create a new view. It is not
     *                      guaranteed that the convertView will have been previously
     *                      created by
     *                      {@link #getChildView(int, int, boolean, android.view.View, android.view.ViewGroup)}.
     * @param parent        the parent that this view will eventually be attached to
     * @return the View corresponding to the child at the specified position
     */
    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        Job job = jobArrayList.get(groupPosition);
        Log log = job.getLogArrayList().get(childPosition);

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.listrow_details_logs, parent, false);
        }

        TextView tv_title = (TextView) convertView.findViewById(R.id.tv_description);
        TextView tv_content = (TextView) convertView.findViewById(R.id.tv_content);
        TextView logDate = (TextView) convertView.findViewById(R.id.tv_created);

        tv_title.setText(log.getLogTitle());
        tv_content.setText(log.getLogText());

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yy");
        logDate.setText(dateFormat.format(new Date(log.getLogDate().getTimeInMillis())));

        return convertView;

    }

    /**
     * Whether the child at the specified position is selectable.
     *
     * @param groupPosition the position of the group that contains the child
     * @param childPosition the position of the child within the group
     * @return whether the child is selectable.
     */
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        //true for editing?
        return true;
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);
    }

    private ArrayList<Job> getJobArrayList() {

        ArrayList<Job> returnList = new ArrayList<>();

        for (Job job : jobArrayList) {

            if (job.getLogArrayList().size() > 0) {
                returnList.add(job);
            }

        }

        return returnList;

    }

}