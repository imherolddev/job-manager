package com.imherolddev.jobmanagerpro.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.imherolddev.jobmanagerpro.NewLog;
import com.imherolddev.jobmanagerpro.R;
import com.imherolddev.jobmanagerpro.model.Job;
import com.imherolddev.jobmanagerpro.model.ListenerUtility;
import com.imherolddev.jobmanagerpro.model.Log;

import java.util.ArrayList;

/**
 * Created by imherolddev on 9/22/2014.
 */
public class LogsFragment extends Fragment implements ExpandableListView.OnChildClickListener {

    private ArrayList<Job> jobArrayList = new ArrayList<>();
    private ArrayList<Log> logArrayList = new ArrayList<>();
    private LogsFragmentAdapter logsFragmentAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        ListenerUtility.JobListListener jobListListener = (ListenerUtility.JobListListener) getActivity();
        jobArrayList = jobListListener.getJobList();
        ListenerUtility.LogListListener logListListener = (ListenerUtility.LogListListener) getActivity();
        logArrayList = logListListener.getLogList();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View convertView = inflater.inflate(R.layout.fragment_logs, container, false);

        ExpandableListView listView_log = (ExpandableListView) convertView.findViewById(R.id.logsList);
        logsFragmentAdapter = new LogsFragmentAdapter(getActivity(), jobArrayList, logArrayList);
        listView_log.setAdapter(logsFragmentAdapter);
        listView_log.setOnChildClickListener(this);

        return convertView;

    }

    /**
     * Callback method to be invoked when a child in this expandable list has
     * been clicked.
     *
     * @param parent        The ExpandableListView where the click happened
     * @param v             The view within the expandable list/ListView that was clicked
     * @param groupPosition The group position that contains the child that
     *                      was clicked
     * @param childPosition The child position within the group
     * @param id            The row id of the child that was clicked
     * @return True if the click was handled
     */
    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

        Log log = (Log) logsFragmentAdapter.getChild(groupPosition, childPosition);
        String jobName = jobArrayList.get(groupPosition).getJobName();

        Intent intent = new Intent(getActivity(), NewLog.class);
        intent.putExtra(NewLog.EDIT_LOG_KEY, log);
        intent.putExtra(NewLog.EDIT_LOG_NAME, jobName);
        intent.putExtra(NewLog.JOB_BUNDLE, bundleJobNames());
        startActivityForResult(intent, NewLog.EDIT_LOG_REQUEST);

        return true;

    }

    //Bundle Job names to AutoCompleTextView in NewLog
    private String[] bundleJobNames() {

        String[] jobNamesArray = new String[jobArrayList.size()];

        for (Job job : jobArrayList) {

            jobNamesArray[jobArrayList.indexOf(job)] = job.getJobName();

        }

        return jobNamesArray;

    }
}
