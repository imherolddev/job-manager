package com.imherolddev.jobmanagerpro;

import android.os.Bundle;
import android.preference.PreferenceActivity;

import com.imherolddev.jobmanagerpro.fragments.MainSettingsFragment;

/**
 * Created by imherolddev on 9/29/2014.
 */
public class MainSettings extends PreferenceActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction()
                .replace(android.R.id.content,
                        new MainSettingsFragment())
                .commit();

    }

}
