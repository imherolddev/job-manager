package com.imherolddev.jobmanagerpro.model.persistence;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by imherolddev on 10/1/2014.
 */
public class JMDBHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "jobManagerDB";

    public static final String JOB_TABLE = "`jobs`";
    public static final String JOB_ID_COL = "_jobID";
    public static final int JOB_ID_COL_POS = 0;
    public static final String JOB_NAME_COL = "jobName";
    public static final int JOB_NAME_COL_POS = 1;

    public static final String DAY_TABLE = "`days`";
    public static final String DAY_ID_COL = "_dayID";
    public static final int DAY_ID_COL_POS = 0;
    public static final String DAY_JOB_ID_COL = "_dayJobID";
    public static final int DAY_JOB_ID_COL_POS = 1;
    public static final String DAY_TIMESTAMP_COL = "timestamp";
    public static final int DAY_TIMESTAMP_COL_POS = 2;

    public static final String LOG_TABLE = "`logs`";
    public static final String LOG_ID_COL =  "_logID";
    public static final int LOG_ID_COL_POS = 0;
    public static final String LOG_JOB_ID_COL = "_logJobID";
    public static final int LOG_JOB_ID_COL_POS = 1;
    public static final String LOG_DATE_COL = "logDate";
    public static final int LOG_DATE_COL_POS = 2;
    public static final String LOG_TITLE_COL = "logTitle";
    public static final int LOG_TITLE_COL_POS = 3;
    public static final String LOG_COL = "log";
    public static final int LOG_COL_POS = 4;


    public JMDBHelper(Context context, String name,
                       SQLiteDatabase.CursorFactory factory, int version) {

        super(context, name, factory, version);

    }

    /**
     * Called when the database is created for the first time. This is where the
     * creation of tables and the initial population of the tables should happen.
     *
     * @param db The database.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {

        String createJobTable =

                "CREATE TABLE IF NOT EXISTS " + JOB_TABLE +
                        "(`" + JOB_ID_COL + "` INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "`" + JOB_NAME_COL + "` VARCHAR(45) UNIQUE NOT NULL);";

        String createDayTable =

                "CREATE TABLE IF NOT EXISTS " + DAY_TABLE +
                        "(`" + DAY_ID_COL + "` INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "`" + DAY_JOB_ID_COL + "` INTEGER NOT NULL, " +
                        "`" + DAY_TIMESTAMP_COL + "` INTEGER NOT NULL);";

        String createLogTable =

                "CREATE TABLE IF NOT EXISTS " + LOG_TABLE +
                        "(`" + LOG_ID_COL + "` INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "`" + LOG_JOB_ID_COL + "` INTEGER NOT NULL, " +
                        "`" + LOG_DATE_COL + "` TIMESTAMP NOT NULL, " +
                        "'" + LOG_TITLE_COL + "' VARCHAR(45) NULL, " +
                        "`" + LOG_COL + "` VARCHAR(500) NOT NULL);";

        db.execSQL(createJobTable);
        db.execSQL(createDayTable);
        db.execSQL(createLogTable);

    }

    /**
     * Called when the database needs to be upgraded. The implementation
     * should use this method to drop tables, add tables, or do anything else it
     * needs to upgrade to the new schema version.
     * <p/>
     * <p>
     * The SQLite ALTER TABLE documentation can be found
     * <a href="http://sqlite.org/lang_altertable.html">here</a>. If you add new columns
     * you can use ALTER TABLE to insert them into a live table. If you rename or remove columns
     * you can use ALTER TABLE to rename the old table, then create the new table and then
     * populate the new table with the contents of the old table.
     * </p><p>
     * This method executes within a transaction.  If an exception is thrown, all changes
     * will automatically be rolled back.
     * </p>
     *
     * @param db         The database.
     * @param oldVersion The old database version.
     * @param newVersion The new database version.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //persist existing, drop
    }
}
