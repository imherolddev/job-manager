package com.imherolddev.jobmanagerpro.model.persistence;

import com.imherolddev.jobmanagerpro.model.Day;
import com.imherolddev.jobmanagerpro.model.Job;
import com.imherolddev.jobmanagerpro.model.Log;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by imherolddev on 10/18/2014.
 */
public interface JMCrudService {

    //Job crud operations
    public boolean createJob(Job job);
    public Job readJob(String jobName);
    public ArrayList<Job> readJobList();
    public boolean updateJob(int position, Job update);
    public boolean deleteJob(int position);

    //Day crud operations
    public boolean createClockTime(int jobId, Calendar calendar);
    public Day readDay(Calendar date);
    public ArrayList<Day> readDayList();
    public boolean updateClockTime(int position, Calendar update);
    public boolean deleteClockTime(int position);

    //Log crud operations
    public boolean createLog(Log log);
    public Log readLog(int logId);
    public ArrayList<Log> readLogList();
    public boolean updateLog(int position, Log log);
    public boolean deleteLog(int position);

}
