package com.imherolddev.jobmanagerpro.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by imherolddev on 8/14/2014.
 */
public class Day {

    /**
     * The id of the Calendar
     */
    private int dayId = 0;
    /**
     * The date of the Day
     */
    private Calendar calendar = new GregorianCalendar();
    /**
     * List of ClockTime's for the Day
     */
    private ArrayList<ClockTime> clockTimeList;
    /**
     * List of job ids
     */
    private ArrayList<Integer> jobIdList;

    /**
     * May be instantiated with
     * @param calendar the calendar to set
     */
    public Day(Calendar calendar) {

        this.calendar.setTimeInMillis(calendar.getTimeInMillis());
        normalize(this.calendar);
        clockTimeList = new ArrayList<>();
        jobIdList = new ArrayList<>();

    }

    /**
     * May be instantiated with first ClockTime
     * @param job the job to set
     * @param calendar the calendar to set
     */
    public Day(Job job, Calendar calendar) {

        clockTimeList = new ArrayList<>();
        addClockTime(job.getJobId(), calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));
        this.calendar.setTimeInMillis(calendar.getTimeInMillis());
        normalize(this.calendar);

    }

    /**
     * Get the day Id
     * @return dayId
     */
    public int getDayId() {
        return dayId;
    }

    /**
     * Set the day id
     * @param id the id to set
     */
    public void setDayId(int id) {
        this.dayId = id;
    }

    /**
     * The date can be changed
     * @param calendar the new date to set
     */
    public void setDate(Calendar calendar) {
        this.calendar.setTimeInMillis(calendar.getTimeInMillis());
        normalize(this.calendar);
    }

    /**
     * Get the date for the Day
     * @return date
     */
    public Calendar getDate() {
        return calendar;
    }

    /**
     * Get the list of ClockTime's
     * @return clockTimeList
     */
    public ArrayList<ClockTime> getClockTimeList() {
        return clockTimeList;
    }

    /**
     * Add a new ClockTime
     * @param jobId the job id to set
     * @param hour the hour to set
     * @param minute the minute to set
     */
    public void addClockTime(int jobId, int hour, int minute) {
        clockTimeList.add(0, new ClockTime(jobId, hour, minute));
    }

    /**
     * Get the latest ClockTime
     * @return ClockTime
     */
    public ClockTime getClockTime(int position) {
        return clockTimeList.get(position);
    }

    /**
     * Get the ClockTimeJob
     */
    public int getClockTimeJob(int postition) {
        return getClockTime(postition).getJobId();
    }

    /**
     * Change a ClockTime in the list
     * @param position the position to change
     * @param hour the hour to set
     * @param minute the minute to set
     */
    public void changeClockTime(int position, int jobId, int hour, int minute) {

        ClockTime newClockTime = new ClockTime(jobId, hour, minute);
        clockTimeList.remove(position);
        clockTimeList.add(position, newClockTime);

    }

    /**
     * Remove a ClockTime from the list
     * @param position the position to remove
     */
    public void removeClockTime(int position) {
        clockTimeList.remove(position);
    }

    /**
     * Get job id list
     */
    public ArrayList<Integer> getJobIdList() {
        return jobIdList;
    }

    /**
     * Add job id to the list
     * @param id the id to set
     */
    public void addJobToList(int id) {
        jobIdList.add(id);
    }

    /**
     * Defines a ClockTime by Integer hour and Integer minute
     */
    public class ClockTime {

        /**
         * The job name
         */
        private int jobId;
        /**
         * The hour for the ClockTime
         */
        private int hour;
        /**
         * The minute for the ClockTime
         */
        private int minute;

        /**
         * Must be instantiated with
         * @param jobId the job id to set
         * @param hour the hour to set
         * @param minute the minute to set
         */
        ClockTime(int jobId, int hour, int minute) {

            this.jobId = jobId;
            this.hour = hour;
            this.minute = minute;

        }

        /**
         * The job name can be changed
         * @param jobId the job id to set
         */
        public void setJobId(int jobId) {
            this.jobId = jobId;
        }

        /**
         * Get the job name for the ClockTime
         * @return jobName
         */
        public int getJobId() {
            return jobId;
        }

        /**
         * The hour can be changed
         * @param hour the hour to set
         */
        public void setHour(int hour) {
            this.hour = hour;
        }

        /**
         * Get the hour for the ClockTime
         * @return hour
         */
        public int getHour() {
            return hour;
        }

        /**
         * The minute can be changed
         * @param minute the minute to set
         */
        public void setMinute(int minute) {
            this.minute = minute;
        }

        /**
         * Geth the minute for the ClockTime
         * @return minute
         */
        public int getMinute() {
            return minute;
        }

    }

    /**
     * Normalizes the time portion of the Calendar to 0, to be used as a date
     * @param timeInMillis the timeInMillis to set
     * @return calendar normalized
     */
    public static Calendar normalize(long timeInMillis) {

        Calendar cal = new GregorianCalendar();
        cal.setTimeInMillis(timeInMillis);
        Day.normalize(cal);

        return cal;

    }

    private static void normalize(Calendar calendar) {

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        calendar.clear();

        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);

    }

}