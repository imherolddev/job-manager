package com.imherolddev.jobmanagerpro.model.persistence;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Calendar;

/**
 * Created by imherolddev on 10/11/2014.
 */
public class PreferenceHelper {

    public static final String CLOCKED_KEY = "isClocked";
    public static final String LAST_JOB_KEY = "lastJob";
    public static final String LAST_DAY_KEY = "lastDay";

    public SharedPreferences preferences;
    public SharedPreferences.Editor editor;

    public PreferenceHelper (Context context) {

        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = preferences.edit();

    }

    public void save(String key, int index) {
        editor.putInt(key, index).commit();
    }

    public void save(String key, boolean value) {
        editor.putBoolean(key, value).commit();
    }

    public void save(String key, Calendar calendar) {
        editor.putLong(key, calendar.getTimeInMillis()).commit();
    }

    public int retrieve(String key) {
        return preferences.getInt(key, 0);
    }

    public boolean retrieveFlag(String key) {
        return preferences.getBoolean(key, false);
    }

    public long retrieveDate(String key) {
        return preferences.getLong(key, 0);
    }

}