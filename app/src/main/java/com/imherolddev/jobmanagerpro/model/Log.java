package com.imherolddev.jobmanagerpro.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by imherolddev on 8/14/2014.
 */
public class Log implements Serializable {

    /**
     * The job name
     */
    private int jobId = 0;
    /**
     * The date of the log
     */
    private Calendar logDate = new GregorianCalendar();
    /**
     * The title of the log
     */
    private String logTitle = "";
    /**
     * The text of the log
     */
    private String logText = "";

    /**
     * Must be instantiated with all attributes
     * @param logDate the log date to set
     * @param logTitle the log title to set
     * @param logText the logText to set
     */
    public Log(Calendar logDate, String logTitle, String logText) {

        this.logDate.setTimeInMillis(logDate.getTimeInMillis());
        this.logTitle = logTitle;
        this.logText = logText;

    }

    /**
     * Get the job name
     * @return jobId
     */
    public int getjobId() {
        return jobId;
    }

    /**
     * Set a new job name
     * @param jobId the job name to set
     */
    public void setjobId(int jobId) {
        this.jobId = jobId;
    }

    /**
     * Get the log date
     * @return logDate
     */
    public Calendar getLogDate() {
        return logDate;
    }

    /**
     * Set a new log date
     * @param logDate the log date to set
     */
    public void setLogDate(Calendar logDate) {
        this.logDate = logDate;
    }

    /**
     * Get the log title
     * @return logTitle
     */
    public String getLogTitle() {
        return logTitle;
    }

    /**
     * Set a new log title
     * @param logTitle the lot title to set
     */
    public void setLogTitle(String logTitle) {
        this.logTitle = logTitle;
    }

    /**
     * Get the log text
     * @return logText
     */
    public String getLogText() {
        return logText;
    }

    /**
     * Set a new log text
     * @param logText the logText to set
     */
    public void setLogText(String logText) {
        this.logText = logText;
    }

}
