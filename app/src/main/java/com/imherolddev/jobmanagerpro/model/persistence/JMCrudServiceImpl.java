package com.imherolddev.jobmanagerpro.model.persistence;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.imherolddev.jobmanagerpro.model.Day;
import com.imherolddev.jobmanagerpro.model.Job;
import com.imherolddev.jobmanagerpro.model.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by imherolddev on 10/18/2014.
 */
public class JMCrudServiceImpl implements JMCrudService {

    private SQLiteDatabase db;
    private Cursor cursor;
    private ContentValues contentValues = new ContentValues();
    private String query;

    private long RESULT= 0;

    public JMCrudServiceImpl(SQLiteDatabase db) {
        this.db = db;
    }

    @Override
    public boolean createJob(Job job) {

        contentValues.clear();

        contentValues.put(JMDBHelper.JOB_NAME_COL, job.getJobName());
        RESULT = db.insert(JMDBHelper.JOB_TABLE, "", contentValues);

        return RESULT != -1;

    }

    @Override
    public Job readJob(String jobName) {

        query = "SELECT * FROM " + JMDBHelper.JOB_TABLE + " WHERE " + JMDBHelper.JOB_NAME_COL + "=?";

        cursor = db.rawQuery(query, new String[] {jobName});

        Job job = null;

        if (cursor.moveToFirst()) {

            do {

                job = new Job(jobName);
                job.setJobId(cursor.getInt(JMDBHelper.JOB_ID_COL_POS));

            } while (cursor.moveToNext());

        }

        return job;

    }

    @Override
    public ArrayList<Job> readJobList() {

        ArrayList<Job> returnList = new ArrayList<>();

        query = "SELECT * FROM " + JMDBHelper.JOB_TABLE + ";";

        cursor = db.rawQuery(query, null);

        if (cursor != null && cursor.moveToFirst()) {

            do {

                Job job = new Job(cursor.getString(JMDBHelper.JOB_NAME_COL_POS));
                job.setJobId(cursor.getInt(JMDBHelper.JOB_ID_COL_POS));
                returnList.add(job);

            } while (cursor.moveToNext());

        }

        return returnList;

    }

    @Override
    public boolean updateJob(int position, Job update) {
        return RESULT != -1;
    }

    @Override
    public boolean deleteJob(int position) {
        return RESULT != -1;
    }

    @Override
    public boolean createClockTime(int jobId, Calendar calendar) {

        contentValues.clear();

        contentValues.put(JMDBHelper.DAY_JOB_ID_COL, jobId);
        contentValues.put(JMDBHelper.DAY_TIMESTAMP_COL, calendar.getTimeInMillis());

        RESULT = db.insert(JMDBHelper.DAY_TABLE, "", contentValues);


        return RESULT != -1;

    }

    @Override
    public Day readDay(Calendar calendar) {
        return null;
    }

    @Override
    public ArrayList<Day> readDayList() {

        Calendar calendar = new GregorianCalendar();
        Calendar lastCalendar = new GregorianCalendar();
        Day day = null;
        ArrayList<Day> returnList = new ArrayList<>();

        query = "SELECT * FROM " + JMDBHelper.DAY_TABLE + " ORDER BY " + JMDBHelper.DAY_TIMESTAMP_COL + " DESC;";

        cursor = db.rawQuery(query, null);

        if (cursor != null && cursor.moveToFirst()) {

            int lastJobId = cursor.getInt(JMDBHelper.DAY_JOB_ID_COL_POS);
            lastCalendar.setTimeInMillis(cursor.getLong(JMDBHelper.DAY_TIMESTAMP_COL_POS));
            day = new Day(lastCalendar);
            day.addJobToList(lastJobId);

            do {

                int jobId = cursor.getInt(JMDBHelper.DAY_JOB_ID_COL_POS);
                calendar.setTimeInMillis(cursor.getLong(JMDBHelper.DAY_TIMESTAMP_COL_POS));

                if (Day.normalize(calendar.getTimeInMillis()).equals(Day.normalize(lastCalendar.getTimeInMillis()))
                        && lastJobId == jobId) {

                    day.addClockTime(jobId, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));

                } else if (Day.normalize(calendar.getTimeInMillis()).equals(Day.normalize(lastCalendar.getTimeInMillis()))
                        && lastJobId != jobId){

                    boolean jobExists = false;
                    for (Integer id : day.getJobIdList()) {
                        if (id == jobId) {
                            jobExists = true;
                            break;
                        }
                    }

                    if (!jobExists) {
                        day.addJobToList(jobId);
                    }

                    day.addClockTime(jobId, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));
                    lastJobId = jobId;

                } else {

                    returnList.add(day);
                    day = new Day(calendar);
                    day.addJobToList(cursor.getInt(JMDBHelper.DAY_JOB_ID_COL_POS));

                }

                lastCalendar.setTimeInMillis(calendar.getTimeInMillis());

            } while (cursor.moveToNext());

            returnList.add(day);

        }

        return returnList;

    }

    @Override
    public boolean updateClockTime(int position, Calendar update) {
        return RESULT != -1;
    }

    @Override
    public boolean deleteClockTime(int position) {
        return RESULT != -1;
    }

    @Override
    public boolean createLog(Log log) {

        contentValues.clear();

        contentValues.put(JMDBHelper.LOG_JOB_ID_COL, log.getjobId());
        contentValues.put(JMDBHelper.LOG_DATE_COL, log.getLogDate().getTimeInMillis());
        contentValues.put(JMDBHelper.LOG_TITLE_COL, log.getLogTitle());
        contentValues.put(JMDBHelper.LOG_COL, log.getLogText());

        RESULT = db.insert(JMDBHelper.LOG_TABLE, "", contentValues);

        return RESULT != -1;

    }

    @Override
    public Log readLog(int logId) {
        return null;
    }

    @Override
    public ArrayList<Log> readLogList() {

        ArrayList<Log> returnList = new ArrayList<>();

        query = "SELECT * FROM " + JMDBHelper.LOG_TABLE + " ORDER BY " + JMDBHelper.LOG_DATE_COL + " DESC;";

        cursor = db.rawQuery(query, null);

        if (cursor != null && cursor.moveToFirst()) {

            Calendar calendar = new GregorianCalendar();

            do {

                calendar.setTimeInMillis(cursor.getLong(JMDBHelper.LOG_DATE_COL_POS));
                Log log = new Log(calendar, cursor.getString(JMDBHelper.LOG_TITLE_COL_POS), cursor.getString(JMDBHelper.LOG_COL_POS));
                log.setjobId(cursor.getInt(JMDBHelper.LOG_JOB_ID_COL_POS));

                returnList.add(log);

            } while (cursor.moveToNext());

        }

        return returnList;

    }

    @Override
    public boolean updateLog(int position, Log log) {
        return RESULT != -1;
    }

    @Override
    public boolean deleteLog(int position) {
        return RESULT != -1;
    }

}
